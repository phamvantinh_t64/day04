
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
    <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
    <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen" />
    <!-- Bootstrap -->
    <!-- Bootstrap DatePicker -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <!-- Bootstrap DatePicker -->
    <script type="text/javascript">
        $(function() {
            $('#txtDate').datepicker({
                format: "dd/mm/yyyy"
            });
        });
    </script>
<title>Registration</title>
<style>
@import url("https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400&display=swap");
</style>
</head>
<body>
<?php
$reg = "/^[0-9]{1,2}\\/[0-9]{1,2}\\/[0-9]{4}$/";
$error = array();
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST["name"])) {
        array_push($error,"Hãy nhập tên.");
    } 
    if (empty($_POST["gender"])) {
        array_push($error,"Hãy chọn giới tính.");
    } 
    if ($_POST["faculty"] == "None") {
        array_push($error,"Hãy chọn phân khoa.");
    }
    if (empty($_POST["birth"])) {
        array_push($error,"Hãy nhập ngày sinh.");
    } elseif (!preg_match($reg,$_POST["birth"])) {
        array_push($error,"Hãy nhập ngày sinh đúng định dạng.");
    }
    
}

echo '<div class="container" style="height: 450px; width: 550px; margin: 15px auto; border: 1px solid #5b9bd5; display: flex; justify-content:center; flex-direction: column; align-items: center;">';
echo '<div style="width: 450px; text-align: left;">';
echo '<label style="color: red">';
foreach($error as $value){
    echo "$value <br>";
}

echo '</label>';
echo '</div>';

echo '<form method="post" action="">'; 
//name
echo '<div style="display: flex; width: 450px; justify-content: space-around; margin: 10px auto">';
echo '<div style="width: 110px; background-color: #5b9bd5; text-align: center; padding: 10px 0; border: 1px solid #42719b;">';
echo '<label style="color: white" >'; echo "Họ và tên ".'<span style = "color:red"> * </span>'; echo '</label>';

echo '</div>';
echo '<input type="text" name="name" style="margin-left: 20px; height: 40px; width: 320px; border: 1px solid #42719b;">';
echo '</div>';
// gender
echo '<div style="display: flex; width: 450px; justify-content: left; margin: 10px auto">';
echo '<div style="width: 109px; background-color: #5b9bd5; text-align: center; padding: 10px 0; border: 1px solid #42719b;">';
echo '<label style="color: white;">'; echo "Giới tính ".'<span style = "color:red"> * </span>'; echo '</label>';
echo '</div>';

echo '<div style="display:flex; align-items: center">';

echo '<input type="radio" id="Nữ" name="gender" style="margin-left:30px;" value="Nữ">';
echo '<label for="Nữ">Nữ</label>';
echo '<input type="radio" id="Nam" name="gender" style="margin-left:30px;" value="Nam">';
echo '<label for="Nam">Nam</label>';

echo '</div>';
echo '</div>';

// Select faculty
echo '<div  style="display: flex; width: 450px; margin: 10px auto">';
echo '<div style="width: 110px; background-color: #5b9bd5; text-align: center; padding: 10px 0; border: 1px solid #42719b;">';
echo '<label style="color: white">'; echo "Phân khoa ".'<span style = "color:red"> * </span>'; echo '</label>';
echo '</div>';	
echo '<select name="faculty" id="faculty" style="margin-left: 20px; height: 40px; width: 170px; border: 1px solid #42719b;">';

echo '<option value="None">'; echo '</option>';
echo '<option value="MAT">'; echo 'Khoa học máy tính'; echo '</option>';
echo '<option value="KDL">'; echo 'Khoa học vật liệu'; echo '</option>';
echo '</select>';
//Date of birth
echo '</div>';
echo '<div  style="display: flex; width: 450px; margin: 10px auto">';
echo '<div style="width: 110px; background-color: #5b9bd5; text-align: center; padding: 10px 0; border: 1px solid #42719b;">';
echo '<label style="color: white" >'; echo 'Ngày sinh '.'<span style = "color:red"> * </span>'; echo '</label>';
echo '</div>';
echo '<input type="text" id="txtDate" name="birth" style="margin-left: 20px; height: 40px; width: 165px; border: 1px solid #42719b;" placeholder="dd/mm/yyyy">';
echo '</div>';



//address
echo '<div  style="display: flex; width: 450px; margin: 10px auto">';
echo '<div style="width: 110px; background-color: #5b9bd5; text-align: center; padding: 10px 0; border: 1px solid #42719b;">';
echo '<label style="color: white" >'; echo 'Địa chỉ '; echo '</label>';
echo '</div>';
echo '<input type="text" name="address" s style="margin-left: 20px; height: 40px; width: 320px; border: 1px solid #42719b;">';
echo '</div>';
echo '<div style="width: 300px; margin: 0 auto; display: flex; justify-content: center;">';
echo '<input type="submit" value="Đăng ký" style="height: 45px; width: 130px; font-size: 15px; background-color: #5b9bd5; border-radius: 5px; color: white;">';
echo '</div>';
echo '</form>';
echo '</div>';
?>
</body>
</html>
